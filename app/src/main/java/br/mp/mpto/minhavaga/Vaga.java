package br.mp.mpto.minhavaga;

import com.google.android.gms.maps.model.LatLng;

public class Vaga {
    private LatLng latLng;
    private String description;
    private String availability;

    public Vaga(LatLng latLng, String description, String availability) {
        this.latLng = latLng;
        this.description = description;
        this.availability = availability;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }
}
