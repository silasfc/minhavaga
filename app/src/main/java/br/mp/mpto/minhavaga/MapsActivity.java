package br.mp.mpto.minhavaga;

import android.app.AlertDialog;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        ArrayList<Vaga> vagas = new WebService().getData();

        for (Vaga v: vagas) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(v.getLatLng());
            markerOptions.title(v.getDescription());
            mMap.addMarker(markerOptions);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(v.getLatLng()));
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.setOnMarkerClickListener(this);
        }

        CameraUpdate zoom = CameraUpdateFactory.newLatLngZoom(vagas.get(0).getLatLng(), 15);
        mMap.moveCamera(zoom);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.i("INFO", marker.getTitle().toString());
        marker.showInfoWindow();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_toast, null);
        builder.setView(dialogView);

        TextView description = (TextView) dialogView.findViewById(R.id.toast_description);
        TextView availability = (TextView) dialogView.findViewById(R.id.toast_availability);
        Button go = (Button) dialogView.findViewById(R.id.toast_go);
        final AlertDialog dialog = builder.create();

        description.setText(marker.getTitle().toString());
        availability.setText("Outras informações");
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MapsActivity.this, "OK", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        dialog.show();

        return true;
    }
}
