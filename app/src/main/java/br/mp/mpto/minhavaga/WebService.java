package br.mp.mpto.minhavaga;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class WebService {

    public ArrayList<Vaga> getData() {
        ArrayList<Vaga> vagas = new ArrayList<>();
        Vaga vaga;

        vaga = new Vaga(new LatLng(-10.184300, -48.328439), "104 Norte, Av. JK", "Disponível");
        vagas.add(vaga);
        vaga = new Vaga(new LatLng(-10.184305, -48.329139), "104 Norte, Av. JK", "Indisponível");
        vagas.add(vaga);
        vaga = new Vaga(new LatLng(-10.184281, -48.326814), "104 Norte, Av. JK", "Disponível");
        vagas.add(vaga);
        vaga = new Vaga(new LatLng(-10.185535, -48.327110), "104 Sul, Rua SE 1", "Indisponível");
        vagas.add(vaga);
        vaga = new Vaga(new LatLng(-10.188234, -48.329107), "104 Sul, Rua SE 1", "Disponível");
        vagas.add(vaga);
        vaga = new Vaga(new LatLng(-10.177791, -48.332518), "102 Norte", "Indisponível");
        vagas.add(vaga);
        vaga = new Vaga(new LatLng(-10.177395, -48.331955), "102 Norte", "Disponível");
        vagas.add(vaga);
        vaga = new Vaga(new LatLng(-10.176667, -48.332143), "202 Norte", "Indisponível");

        vagas.add(vaga);

        return vagas;
    }
}
